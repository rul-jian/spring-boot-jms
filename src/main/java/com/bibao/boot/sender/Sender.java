package com.bibao.boot.sender;

public interface Sender {
	public void send(Object message);
}
